import math

class Piece:
    def __init__(self, obj, lanes):
        self.type = ""
        self.curve = []
        self.length = []
        self.radius = 0
        self.angle = 0
        self.switch = False
        if "switch" in obj:
            self.switch = obj["switch"]
        if "radius" in obj:
            self.type = "curve"
            for i in lanes:
                i = i["distanceFromCenter"]
                self.radius = obj["radius"]
                self.angle = obj["angle"]
                length = abs(self.angle / 360 * math.pi * (self.radius + i))
                self.length.append(length)
                self.curve.append(self.angle / length)
        else:
            self.length.append(obj["length"])

    def __str__(self):
        if self.type != "curve":
            return "Straight piece of length {0}, switch: {1}".format(self.length[0], self.switch)
        return ("Curved piece, {0} lanes, lengths: {1}, curves: {2}, rad: {3}, ang: {4}, switch: {5}".format(
            len(self.length), self.length, self.curve, self.radius, self.angle, self.switch))

